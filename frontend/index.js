// Main
import React from 'react';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux'
import { render } from 'react-dom';

import Graph from './containers/Graph'
import AppReducer from './reducers/AppReducer'
import GraphsReducer from './reducers/GraphReducer'
import * as graphUtils from './utils/Utils'

let rootReducer = combineReducers({
    'app': AppReducer,
    'graphs' : GraphsReducer
});

const initialState = {
    graphs: [
        {id: 'source-hddtemp-nas'},
        {id: 'source-systemload-nas'},
    ]
};

let store = createStore(rootReducer, initialState);

// TODO create a dash component which renders the graphs
render(
    <Provider store={store}>
        <div className="container-fluid">
            <div className="page-header">
                <h1>Dashboard</h1>
            </div>
            <div className="container-fluid">
                <div className="row">
                    <Graph metricSourceId="source-hddtemp-nas" title="hddtemp@NAS"/>
                </div>
                <div className="row">
                    <Graph metricSourceId="source-systemload-nas" title="loadavg@NAS"/>
                </div>
            </div>
        </div>
    </Provider>
    ,
    document.getElementById("root")
);

// Initialize graphs with available sources
graphUtils.fetchSources((sources) => {
    sources.forEach((source) => {
        console.log('Fetching data for source:', source);
        let sourceId = source.source_id;
        let sourceInfo = graphUtils.convertMetricSourceData(source);

        // lets get metrics data for a given sourceId
        console.log('Fetching metrics for source:', sourceId);
        graphUtils.fetchMetrics(sourceId, (metrics) => {
            let data = graphUtils.convertData(metrics, sourceInfo.metricIdList);

            let eventData = {
                id: sourceId,
                type: 'graph/update',
                data: {
                    legend: sourceInfo.legend,
                    values: data
                }
            };
            console.log("Got metrics, updating graph for:", sourceId);
            store.dispatch(eventData);
        });
    });
});
