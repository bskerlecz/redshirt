import React, { PropTypes } from 'react'
import MetricsGraphics from 'react-metrics-graphics'
import * as d3 from 'd3'


const GraphView = ({metricSourceId, title, description, data}) => {

    console.log('update graph-view', data);

    let dimensions = {
        width: 640,
        height: 250
    };

    if (data == undefined) {
        return (
            <MetricsGraphics
                title       = { title }
                description = { description==undefined ? '' : description }
                chart_type  = { 'missing-data' }
                missing_text= { 'Loading..' }
                width       = { dimensions.width }
                height      = { dimensions.height }
            />
        );
    } else {
        return (
            <MetricsGraphics
                title       = { title }
                chart_type  = { 'line' }
                description = { description==undefined ? '' : description }
                data        = { data.values }
                legend      = { data.legend }
                width       = { dimensions.width }
                height      = { dimensions.height }
                min_y_from_data = { true }
                aggregate_rollover = { true }
                x_accessor  = { 'date' }
                y_accessor  = { 'value' }
                right       = { 60 }
                left        = { 60 }
            />
        );
    }
};

GraphView.propTypes = {
    metricSourceId: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
    data: PropTypes.object
};

export default GraphView;