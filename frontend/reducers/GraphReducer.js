
const GraphsReducer = (graphs = [], action) => {
    console.log('graphs-reducer', graphs, action);

    return graphs.map((graph) => {

        console.log('graphs-reducer iterate', graph);

        if (action.id == graph.id) {
            return GraphReducer(graph, action);
        } else {
            return graph;
        }
    });
};

const GraphReducer = (graph = [], action) => {
    console.log('graph-reducer', action);
    switch (action.type) {
        case 'graph/update':
            console.log('will update');
            let data = Object.assign({}, graph, {
                data: action.data
            });

            console.log('graph-update-with-data', action.id, data);
            return data;
        default:
            return graph;
    }
};

export default GraphsReducer;
