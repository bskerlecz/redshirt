// TODO make api server url configurable
const API_SERVER_URL = 'http://localhost:8000';

import fetch from 'isomorphic-fetch';

const fetchMetrics = (sourceId, handler) => {
    fetch(API_SERVER_URL + '/metric/' + sourceId)
        .then((response) => {
            if(response.status >= 400) {
                console.log(response.status, response);
                throw Error("Cannot fetch url.");
            }
            return response.json();
        })
        .then((json) => {
            console.log('response', json);
            handler(json);
        });
};

const fetchSources = (update) => {
    fetch(API_SERVER_URL + '/source')
        .then((response) => {
            if(response.status >= 400) {
                console.log(response.status, response);
                throw Error("Cannot fetch url.");
            } else {
                return response.json();
            }
        })
        .then((json) => {
            console.log('response', json);
            update(json);
        });
};

const convertMetricSourceData = (sourceData) => {
    let data = {
        legend: [],
        metricIdList: []
    };

    let source = sourceData.data;
    source.forEach((row) => {
        data.legend.push(row.title);
        data.metricIdList.push(row.id);
    });

    return data;
};

const convertData = (metrics, metricIdList) => {
    let converted = [];
    let metricsRows = {};

    // Ectract metrics from raw data into separate rows by metricsId
    metrics.forEach((row) => {
        let ts = Math.floor(row.ts / 1000);
        let d = new Date();
        d.setSeconds(ts);

        if (metricsRows[row.metric_id] == undefined) {
            metricsRows[row.metric_id] = []
        }

        metricsRows[row.metric_id].push({
            'date'   : d,
            'value' : row.value
        });

    });

    // Put requested metrics into converted array
    metricIdList.forEach((metricId) => {
        converted.push(metricsRows[metricId]);
    });

    return converted;
};
export {
    convertData,
    convertMetricSourceData,
    fetchMetrics,
    fetchSources
}