import React, { PropTypes } from 'react'
import { connect } from 'react-redux';
import GraphView from '../components/GraphView'


const mapStateToProps = (state, ownProps) => {
    let currentGraphData = null;

    state.graphs.forEach((graphData) => {
        if (graphData.id == ownProps.metricSourceId) {
            currentGraphData = graphData;
        }
    });

    return {
        data: currentGraphData.data
    };
};

const mapDispatchToProps = (dispatch) => {
    return {};
};

const Graph = connect(
    mapStateToProps,
    mapDispatchToProps,
)(GraphView);

export default Graph;
