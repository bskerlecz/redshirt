var path 				= require('path');
var webpack 			= require('webpack');
var htmlWebpackPlugin 	= require('html-webpack-plugin');

module.exports = {
	devtool: 'eval-source-map',
	entry: [
		'webpack-dev-server/client?http://localhost:8080',
		'webpack/hot/only-dev-server',
		'react-hot-loader/patch',
		path.join(__dirname, 'frontend/index.js')
	],
	output: {
		path: path.join(__dirname, '/dist/frontend/'),
		filename: '[name].js',
		publicPath: '/'
	},
	plugins: [
		new htmlWebpackPlugin({
			template: 'frontend/index.tpl.html',
			inject: 'body',
			filename: 'index.html'
		}),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin(),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify('development')
		})
	],
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: [/node_modules/, /dist/],
				loader: 'babel',
				query: { presets: [ 'es2015', 'react' ] }

			},
			{
				test: /\.css$/,
				exclude: [/node_modules/, /dist/],
				loaders: [
				    'style?sourceMap',
                                    'css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'
                ]
			}
		]
	}
};
