#RedShirt v0.2 - under development
NodeJS server/client for collecting various metrics, like services and system resources data, eg. temperature, service status, system load, etc. Plus and additional dashboard for showing collected metrics.

###Data collector server/client (EcmaScript 6/NodeJS) -
- backend/api: REST/JSON Api for retreiving collected data
- backend/client: client libraries
- backend/collector: data collector server
- backend/sender: data collector clients (HddTemp: hdd temperature data, SystemLoad: system load data)
- backend/examples: example clients

###Dashboard for showing collected metrics (Redux/React)  
- frontend
![alt text](https://bytebucket.org/bskerlecz/redshirt/raw/a1be249e0d565846b740e9f30b9d17c4da48a198/dash-screen-v_0_2.png "Sample dashboard")

## Requirements
- NodeJS
- MongoDB

###Install and run
- First, install package dependencies with `npm install`
- To build the whole backend, run: `npm run backend-build`
- To run the Collector server: `npm run collector-server`
- API server: `npm run api-server` (default address: http://localhost:8000)
- Backend developer mode: `npm run backend-dev`
- Dashboard developer mode: `npm run frontend-dev` (default address: http://localhost:8080)

###Additional information

- Client data format

`MetricsSource`
```
{

    id: 'computer-1-hdd-temperatures',
    metrics: 
    [
        {
            id: 'hdd-1',
            title: '/dev/sda',
            description: 'Drive 1 temperature'
            values: ['timestamp', 'celsius']
        },
        {
            id: 'hdd-2',
            title: '/dev/sdb',
            description: 'Drive 2 temperature'
            values: ['timestamp', 'celsius']
        },
        {
            id: 'hdd-3',
            title: '/dev/sdc',
            description: 'Drive 3 temperature'
            values: ['timestamp', 'celsius']
        },   
    ] 
}
```
`Metrics`
```
{
    sourceId: 'computer-1-hdd-temperatures',
    metrics: [
        {
            metricId: 'hdd-1',
            values: [
                [1, 23], [2, 23], [3, 24], [4, 25], [5, 25], [6, 24]
            ]
        },
        {
            metricId: 'hdd-2',
            values: [
                [1, 23], [2, 23], [3, 24], [4, 25], [5, 25], [6, 24]
            ]
        },
         {
             metricId: 'hdd-3',
             values: [
                 [1, 23]
             ]
         }

    ]
}
```
