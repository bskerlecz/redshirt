var Webpack		= require('webpack');
var Server		= require('webpack-dev-server');
var config		= require('./webpack.config');

new Server(Webpack(config), {
	publicPath: config.output.publicPath,
	hot: true,
	historyApiFallback: true,
	quiet: false,
	noInfo: false,
	stats: {
		assets: false,
		colors: true,
		version: true,
		hash: true,
		chunks: false,
		chunkModules: false
	}
}).listen(8080, 'localhost', function(err) {
	if (err) {
		console.log(err);
	}
	console.log('Listening on "localhost:8080"');
});
