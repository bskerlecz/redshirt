module.exports.id = "metrics-format-v2";

const METRICS_COLLECTION_PREFIX = 'metrics_source_';

const migrateCollection = function(migration, collectionName, cb) {
    migration.log("* Processing " + collectionName);
    var c = migration.db.collection(collectionName);

    var cursor = c.find({"data.0.value.0": { $gt: 0 }});
    var processed = 0;
    var created = 0;

    cursor.forEach(function(row) {
        // next
        var source_id = row.source_id;

        row.data.forEach(function(metric) {
            var new_row = {
                'source_id': source_id,
                'metric_id': metric.id,
                'ts': metric.value[0],
                'value': metric.value[1]
            };

            c.insertOne(new_row);
            created++;
        });

        c.deleteOne({"_id": row._id});
        processed++;

    }, function(err) {
        // end
        if (err) {
            cb(err);
        }

        migration.log(
            [
                "+ Migration completed for collection: " + collectionName,
                "records removed: " + processed,
                "records added: " + created
            ].join(",")
        );

        cb();
    });
};

module.exports.up = function (done) {
    // use this.db for MongoDB communication, and this.log() for logging
    var self = this;
    this.log("Migrating log tables into version 2 format.");

    this.db.listCollections().toArray(function(err, collections) {
        if (err) {
            done(err);
        }

        if (collections.length == 0) {
            done(new Error("No collections found."));
        }

        var processing = 0;

        while (true) {
            var collection = collections.pop();
            if (collection == undefined) {
                break;
            }

            var migrate = collection.name.substr(0, METRICS_COLLECTION_PREFIX.length) == METRICS_COLLECTION_PREFIX;
            if (!migrate) {
                continue;
            }

            processing++;
            migrateCollection(self, collection.name, function(err) {
                processing--;

                if (err) {
                    done(err);
                } else {
                    if (collections.length == 0 && processing == 0) {
                        done();
                    }
                }
            });
        }
    });
};

module.exports.down = function (done) {
    done(new Error("Reverting this migration is not supported."));
};