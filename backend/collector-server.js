import Collector from './collector/Collector';
import Storage from './collector/Storage';

let mongoStorage = new Storage({ db: 'dc_store' });
let server = new Collector({ 'storage': mongoStorage });

server.run();
