import Client from '../client/Client'
import HddTemp from '../sender/HddTemp'

let client = new Client();
let sender = new HddTemp({client: client});

sender.send('example-hddtemp-source-1');
