import * as assert from 'assert';
import { exec } from 'child_process';
import { Socket } from 'net';
import * as crypt from 'crypto';

class HddTemp {
    constructor(params) {
        assert.ok(params != undefined, "Missing required argument 'params'.");
        assert.ok(params.client != undefined, "Missing required property 'client'.");

        this.client = params.client;
        this.log = (message, error) => {
            console.log(message, error);
        };

        this.hddtempCli = params.cli != undefined ? params.cli : 'hddtemp';
        this.hddtempDaemonParams = {
            'host': 'localhost',
            'port': 7634
        };
    }

    static trimChar(value, char) {
        if (value[0] == char) { value = value.slice(1); }
        if (value[-1] == char) { value = value.slice(0, -1); }

        return value;
    }

    async sendData(id, data) {
        let metricSource = { id: id, metrics: [], type: 'source'};
        let metricData = { id: id, metrics: [] };

        data.forEach((v) => {
            let metricsId = crypt.createHash('md5').update(v.device + v.name).digest('hex');

            // Populate metrics-source
            metricSource.metrics.push({
                id: metricsId,
                title: v.device,
                description: v.name,
                valueLabels: ['timestamp', 'celsius']
            });

            // populate metrics data
            metricData.metrics.push({
                id: metricsId,
                value: [v.timestamp, v.temperature]
            });
        });

        try {
            await this.client.send(metricSource);
        } catch (error) {
            this.log('cannot save metrics-source', error);
        }

        try {
            await this.client.send(metricData);
        } catch (error) {
            this.log('cannot save metrics-data', error);
        }
    }

    collectData(output, lineSeparator = "\n", wordSeparator = ":") {
        let timestamp = new Date().getTime();
        let rows = output.split(lineSeparator);
        let data = [];

        rows.forEach((row) => {
            let items = HddTemp.trimChar(row, wordSeparator).split(wordSeparator);
            if (items.length >= 3) {
                let device  = items[0].trim();
                let name = items[1].trim();
                let temperature = items[2].trim().replace(/°C/, '');
                if (temperature.indexOf("available") == -1 && temperature != "NA") {
                    data.push({
                        device: device,
                        name: name,
                        temperature: parseFloat(temperature),
                        timestamp: timestamp
                    });
                }
            }
        });

        return data;
    }

    static get(params = {}) {
        return new Promise((resolve, reject) => {
            const socket = new Socket();
            let responseStr = "";

            socket
                .connect(params)
                .on('error', (error) => { reject(error);})
                .on('data', (data) => {
                    responseStr += data;
                })
                .on('close', () => {
                    resolve(responseStr);
                })
        });
    }

    static exec(command) {
        return new Promise((resolve, reject) => {
            exec(command, (error, stdout, stderr) => {
                if (error!=null) {
                    reject(error);
                }
                resolve(stdout);
            }).on('error', (error) => {
                reject(error);
            });
        });
    }

    async _send(metricSourceId) {
        let tryDaemon = false;

        try {
            let stdout = await HddTemp.exec(this.hddtempCli);
            let data = this.collectData(stdout.toString());
            if (data.length == 0) {
                throw new Error("No data to send.");
            }
            await this.sendData(metricSourceId, data);
        } catch (error) {
            this.log('Send failed (hddtemp cli)', error);
            tryDaemon = true;
        }

        if (tryDaemon) {
            try {
                let output = await HddTemp.get(this.hddtempDaemonParams);
                this.log("Response from HddTemp daemon:", output.toString());
                let data = this.collectData(output.toString(), "||", "|");
                if (data.length == 0) {
                    throw new Error("No data to send.");
                }
                await this.sendData(metricSourceId, data);
            } catch (error) {
                this.log('Send failed (hddtemp daemon)', error);
            }
        }
    }

    send(metricSourceId) {
        assert.ok(metricSourceId != undefined, "Missing required argument 'metricSourceId'");
        this._send(metricSourceId);
    }
}

export default HddTemp;