import * as assert from 'assert';
import { exec } from 'child_process';
import { Socket } from 'net';
import * as crypt from 'crypto';
import { readFileSync } from 'fs';

class SystemLoad {

    constructor(params) {
        assert.ok(params != undefined, "Missing required argument 'params'.");
        assert.ok(params.client != undefined, "Missing required property 'client'.");

        this.client = params.client;
        this.log = (message, error) => {
            console.log(message, error);
        };

    }

    async sendData(id, data, timestamp) {
        let metricSource = { id: id, metrics: [], type: 'source'};
        let metricData = { id: id, metrics: [] };

        let index = 0;

        let metricLabels = [
            { title: '1',   description: '1 min average' },
            { title: '5',   description: '5 min average' },
            { title: '15',  description: '15 min average' }
        ];

        data.forEach((v) => {
            let title = metricLabels[index].title;
            let desc = metricLabels[index].description;

            let metricsId = crypt.createHash('md5').update(title).digest('hex');

            // Populate metrics-source
            metricSource.metrics.push({
                id: metricsId,
                title: title,
                description: desc,
                valueLabels: ['timestamp', 'load']
            });

            // populate metrics data
            metricData.metrics.push({
                id: metricsId,
                value: [timestamp, v]
            });

            index++;
        });


        try {
            console.log('metric-source to be send:', JSON.stringify(metricSource));
            await this.client.send(metricSource);
        } catch (error) {
            this.log('cannot save metrics-source', error);
        }

        try {
            console.log('metric-data to be send:', JSON.stringify(metricData));
            await this.client.send(metricData);
        } catch (error) {
            this.log('cannot save metrics-data', error);
        }
    }

    parseLoadAvg(output) {
        let data = [];

        if (output!=undefined) {
            let items = output.split(" ");
            if (items.length > 3) {
                data.push(parseFloat(items[0])); // 1 min avg
                data.push(parseFloat(items[1])); // 5 min avg
                data.push(parseFloat(items[2])); // 15 min avg
            }
        }

        return data;
    }

    async _send(metricSourceId) {
        const systemLoadFile = '/proc/loadavg';

        try {
            let timestamp = new Date().getTime();
            let output = readFileSync(systemLoadFile);
            let values = this.parseLoadAvg(output.toString());
            await this.sendData(metricSourceId, values, timestamp);
        } catch (error) {
            this.log('Send failed', error);
        }
    }

    send(metricSourceId) {
        assert.ok(metricSourceId != undefined, "Missing required argument 'metricSourceId'");
        this._send(metricSourceId);
    }

}

export default SystemLoad;
