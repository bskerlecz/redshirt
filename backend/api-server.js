import Server       from './api/Server';
import { Logger }   from './utils/Logger';
import Storage      from './collector/Storage';

// Lets create dependencies
let consoleLogger   = new Logger();
let mongoStorage    = new Storage({ db: 'dc_store', logger: consoleLogger });

// Lets create server and inject dependencies and params
let server = new Server({
    port: 8000,
    logger: consoleLogger,
    storage: mongoStorage
});

// Start server
server.run();
