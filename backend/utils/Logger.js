// Simple console based logger
const LOGLEVEL = {
    INFO: 'info',
    WARNING: 'warn',
    DEBUG: 'debug',
    ERROR: 'error'
};

class Logger {
    constructor() {
        // Set default logger function to console.log()
        this.loggerFn = (row) => {
            console.log(row);
        }
    }

    log(logLevel, message) {
        let rowParts = [new Date().toString(), '[' + logLevel + ']', message];
        this.loggerFn(rowParts.join(' '));
    }

    info(message) {
        this.log(LOGLEVEL.INFO, message);
    }

    warning(message) {
        this.log(LOGLEVEL.WARNING, message);
    }

    debug(message) {
        this.log(LOGLEVEL.DEBUG, message);
    }

    error(message) {
        this.log(LOGLEVEL.ERROR, message);
    }
}

export {
    Logger,
    LOGLEVEL
}
