import { readFileSync } from 'fs';

class SeriesReader {

    static read(fileName) {
        var contents = readFileSync(fileName);
        return JSON.parse(contents)
    }
}

export default SeriesReader;