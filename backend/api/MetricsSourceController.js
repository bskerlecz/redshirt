import * as assert from 'assert';

/**
 * @property {Logger} logger
 * @property {Storage} storage
 */
class MetricsSourceController {

    constructor(params) {
        assert.ok(params !== undefined, "Required argument 'params'.");
        assert.ok(params.storage !== undefined, "Invalid or missing 'storage'.");

        this.storage = params.storage;

        if (params.logger !== undefined) {
            this.logger = params.logger;
        }

        return this;
    }

    /**
     * @param params
     */
    index(params) {
        return new Promise((resolve, reject) => {
            if (this.logger) this.logger.debug([this.constructor.name, 'index'].join(" "));

            this.storage.listSource().then((data) => {
                resolve(data);
            }, () => {
                resolve([]);
            });
        });
    }
}

export default MetricsSourceController;