class RequestHandler {

    constructor(response) {
        this.response = response;
    }

    handle(controller, action, params) {
        if (action === undefined) {
            // Set default action to 'index'
            action = 'index';
        }

        if (controller[action] === undefined) {
            this.send503();
        } else {
            controller[action](params).then((data) => {
                this.sendJSON(data);
            }, () => {
                this.send503();
            });
        }
    }

    send503() {
        this.response
            .writeHead(503, {
            'Content-Type': 'text/plain',
            'Access-Control-Allow-Origin': '*'
            });

        this.response
            .end('Internal Server error.');
    }

    sendJSON(data) {
        this.response
            .writeHead(200, {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            });

        this.response
            .end(JSON.stringify(data));
    }
}

export default RequestHandler;