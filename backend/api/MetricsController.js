import * as assert from 'assert';

/**
 * @property {Logger} logger
 * @property {Storage} storage
 */
class MetricsController {

    constructor(params) {
        assert.ok(params !== undefined, "Required argument 'params'.");
        assert.ok(params.storage !== undefined, "Invalid or missing 'storage'.");

        this.storage = params.storage;

        if (params.logger !== undefined) {
            this.logger = params.logger;
        }

        return this;
    }

    /**
     * Accepted parameters:
     *  - id*: source-id
     *  - date: formatted date (eg. 2017-12-30)
     *  - interval: number of days (eg. 7d)
     *
     * @param params
     * @returns {Promise}
     */
    index(params) {
        return new Promise((resolve, reject) => {
            if (this.logger) this.logger.debug([this.constructor.name, 'index'].join(" "));

            if (params.id === undefined) {
                reject("Missing required parameter 'id'.");
            }

            this.storage.listMetrics(params.id).then((data) => {
                resolve(data);
            }, () => {
                resolve([]);
            });
        });
    }
}

export default MetricsController;