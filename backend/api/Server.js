import * as assert from 'assert';
import { createServer } from 'http';
import * as url from 'url';
import { LOGLEVEL } from '../utils/Logger'
import * as fs from 'fs'
import * as path from 'path'
import * as controllers from './Controllers';
import * as querystring from 'querystring';
import ResponseHandler from './RequestHandler'

class Server {
    constructor(params) {
        assert.ok(params !== undefined, "Required argument 'params'.");
        assert.ok(params.storage !== undefined, "Invalid or missing 'storage'.");

        this.storage = params.storage;

        if (params.logger !== undefined) {
            this.logger = params.logger;
        }

        this.staticPath = __dirname;
        this.port = params.port===undefined ? 8080 : params.port;

        this.srv =
            createServer((req, res) => {
                    this.handler(req, res)
                })
                .on('clientError', (err, socket) => {
                    socket.end('HTTP/1.1 400 Bad Request\r\n\r\n')
                });
    }

    log(logLevel, message) {
        if (this.logger!=undefined) {
            this.logger.log(logLevel, message);
        }
    }


    run() {
        this.log(LOGLEVEL.INFO, "Server running on address: http://localhost:" + this.port);
        this.log(LOGLEVEL.INFO, "Configuration: " + JSON.stringify({
                'staticPath': this.staticPath
            }));

        this.srv.listen(this.port);
    }

    static match(value, pattern, matches) {
        let res = value.match(pattern);
        if (res !== null) {
            if (matches === undefined) {
                matches = {};
            }
            matches.result = res;
            return true;
        }

        return false;
    }

    handler(req, res) {
        let handler     = new ResponseHandler(res);
        let _url        = url.parse(req.url);
        let requestPath = _url.pathname;
        let queryParams = querystring.parse(_url.query);
        let logData     = [req.socket.remoteAddress, req.method, req.url, JSON.stringify(req.headers)];
        let matches     = {};
        let params, controller, action;

        this.log(LOGLEVEL.INFO, logData.join(" "));

        switch (true) {
            // -> GET /source
            case Server.match(requestPath, /^\/source/):
                params = {};
                params = Object.assign(queryParams, params);

                controller = new controllers.MetricsSourceController({ logger: this.logger, storage: this.storage});
                action = 'index';

                handler.handle(controller, action, params);
                break;
            // -> GET /metric/{metric-source-id}
            case Server.match(requestPath, /^\/metric\/(.*)/, matches):
                params = { 'id': matches.result[1] };
                params = Object.assign(queryParams, params);

                controller = new controllers.MetricsController({ logger: this.logger, storage: this.storage});
                action = 'index';

                handler.handle(controller, action, params);
                break;
            // -> GET /
            case requestPath == '/':
                handler.handle((params, send) => {
                    send({
                        'api-name': 'redshirt',
                        'api-version': '1.0'
                    });
                });
                break;
            default:
                let result = this.resolveStatic(requestPath);
                if (result !== false) {
                    this.sendStatic(result.file, res);
                } else {
                    this.send404(res);
                }
        }
    }

    resolveStatic(requestPath) {
        // TBD switch from sync to async

        let normalized = path.normalize(requestPath);
        if (normalized.substr(0,1) == path.sep) {
            normalized = normalized.substr(1);
        }

        let filePath = path.join(this.staticPath, normalized);

        if (fs.existsSync(filePath)) {
            return {
                file: filePath
            }
        }

        return false;
    }

    /**
     *
     * @param {String} filePath
     * @param res
     */
    sendStatic(filePath, res) {
        // TODO: do async
        // TODO: get proper mime-type
        this.log(LOGLEVEL.INFO, [200, filePath].join(" "));

        let stat = fs.statSync(filePath);


        res.writeHead(200, {
            'Content-Type': 'application/octet-stream',
            'Content-Length': stat.size
        });

        let stream = fs.createReadStream(filePath);
        stream.pipe(res);
    }

    send404(res) {
        res.writeHead(404, {
            'Content-Type': 'text/plain',
            'Access-Control-Allow-Origin': '*'
        });
        res.end("Not found.");
    }

    send(data, res) {
        res.writeHead(200,
            {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            }
        );
        res.end(JSON.stringify(data));
    }
}

export default Server;