import { createServer } from 'net';
import * as assert from 'assert';

class Collector {

    constructor(params) {
        assert.ok(params !== undefined, "Required argument 'params'.");
        assert.ok(params.storage !== undefined, "Invalid or missing 'storage'.");

        console.log("Collector created.");

        this.storage = params.storage;

        this.address    = params.address===undefined ? 'localhost' : params.address;
        this.port       = params.port===undefined ? 8888 : params.port;

        this.srv = createServer((socket) => { this.handler(socket) })
            .on('error', (error) => { Collector.error(error) });
    }

    run() {
        let params = { host: this.address, port: this.port };
        this.srv.listen(params, () => { this.listen() });
    }


    async receive(data, params) {
        let jsonStr = data.toString();
        let json = JSON.parse(jsonStr);
        let result = false;

        console.log(params.date, params.address, jsonStr);

        if (json.type == undefined) {
            result = await this.storage.saveMetrics(json.id, json.metrics);
        } else {
            result = await this.storage.saveSource(json.id, json.metrics)
        }

        return result;
    }

    handler(socket) {
        console.log('handle connection');

        let params = {
            address: socket.remoteAddress,
            date: new Date().toString()
        };

        socket.on('data', async (data) => {
            let result = await this.receive(data, params);

            if (result) {
                socket.write("Ok");
            } else {
                socket.write("Error");
            }
        });

    }

    static error(e) {
        console.log(e);
    }

    listen() {
        console.log("Collector running on " + this.address + ":" + this.port);
    }
}

export default Collector;