import { MongoClient } from 'mongodb';
import * as assert from 'assert';
import { LOGLEVEL } from '../utils/Logger'
/**
 * MongoDB storage implementation
 *
 * @property {Logger} logger
 *
 */
class Storage {

    constructor(params) {
        assert.ok(params !== undefined, "Required argument 'params'.");
        assert.ok(params.db !== undefined, "Invalid or missing parameter 'db'.");

        if (params.logger !== undefined) {
            this.logger = params.logger;
        }

        this.log = (message, error) => {
            if (this.logger !== undefined) {
                this.logger.debug([message.toString(), error === undefined ? '' : error.toString()].join(" "));
            } else {
                console.log(message, error === undefined ? '' : error);
            }
        };

        this.db = params.db;
        this.host = params.host === undefined ? 'localhost' : params.host;
        this.port = params.port === undefined ? '27017' : params.port;

        this.url = ['mongodb://', this.host, ':', this.port, '/', this.db].join("");

        this.log('Storage connection:', this.url);
    }

    static sourceCollectionName() {
        return 'metrics_source';
    }

    static metricsCollectionName(sourceId) {
        return ['metrics', sourceId.replace(/-/g,'_')].join('_');
    }

    async listSource() {
        this.log("list-metrics-source");
        try {
            let db          = await MongoClient.connect(this.url);
            let collection  = await db.collection(Storage.sourceCollectionName());
            let list        = await collection.find();
            let data        = await list.toArray();

            db.close();

            return data;
        } catch (error) {
            this.log('Cannot get metrics source list', error);
            return [];
        }
    }

    async listMetrics(sourceId) {
        // TODO add parameters for filtering
        // TODO add automatic averaging for queries with large intervals
        this.log("list-metrics for " + sourceId);
        try {
            let db          = await MongoClient.connect(this.url);
            let collection  = await db.collection(Storage.metricsCollectionName(sourceId));
            // TODO update query with the new data format
            let list        = await collection.find({'source_id': sourceId}).sort({'data.0.value.0': 1});
            let data        = await list.toArray();

            db.close();

            return data;
        } catch (error) {
            this.log('Cannot get metrics list for source: ' + sourceId, error);
            return [];
        }
    }

    async saveSource(sourceId, data) {
        let row = {
            source_id: sourceId,
            data: data
        };

        this.log('saving data', row);

        try {
            let db = await MongoClient.connect(this.url);
            let collection = await db.collection(Storage.sourceCollectionName());
            let source = await collection.findOne({source_id: sourceId});
            if (source == null) {
                let result = await collection.insertOne(row);
            }

            await db.close();
            return true;
        } catch (error) {
            this.log('Cannot save metrics source', error);
            return false;
        }
    }

    /*
     Sample format for client data:

     {
     "id": "source-foo-xxx",
     "metrics": [
     {"id": "metrics-1-id", value: [a,b]},
     {"id": "metrics-2-id", value: [c,d]},
     ]
     }
     */
    async saveMetrics(sourceId, data) {
        let rows = [];
        data.forEach(function(data_row) {
            rows.push({
                source_id: sourceId,
                metric_id: data_row.id,
                ts: data_row.value[0],
                value: data_row.value[1],
            })
        });

        this.log('saving data', rows);

        try {
            let db = await MongoClient.connect(this.url);
            let sourceColl = await db.collection(Storage.sourceCollectionName());
            let result = await sourceColl.findOne({source_id: sourceId});

            if (result == null) {
                throw new Error("No metrics-source defined for sourceId: " + sourceId);
            }

            let metricsColl = await db.collection(Storage.metricsCollectionName(sourceId));
            result = await metricsColl.insertMany(rows);

            await db.close();

            return true;
        } catch (error) {
            this.log('Cannot save metrics data', error);
            return false;
        }
    }
}

export default Storage;