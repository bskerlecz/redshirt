import SeriesReader from './utils/SeriesReader'
import { join } from 'path';
import * as p from 'process';

if (p.argv.length < 3) {
    console.log("Missing required parameter: 'filename'");
    p.exit();
}

let fileName = p.argv[2];
let contents = SeriesReader.read(fileName);

console.log("Series found:", contents.series.length);
