import { Socket } from 'net';

class Client {

    constructor(address, port) {
        console.log("Client created.");

        this.address = address==undefined ? 'localhost' : address;
        this.port = port==undefined ? 8888 : port;

        this.log = (message, error) => {
            console.log(message, error==undefined ? '':error);
        };
    }


    sendData(data) {
        let params = { host: this.address, port: this.port };
        return new Promise((resolve, reject) => {
            const socket = new Socket();

            socket
                .connect(params, () => {
                    this.log('data', data);
                    socket.write(JSON.stringify(data));
                })
                .on('data', (data) => {
                    socket.end();
                    resolve(data.toString())
                })
                .on('error', (error) => {
                    this.log('error', error);
                    reject(error);
                })
                .on('close', () => {
                    this.log('connection closed')
                });

        });
    }

    async send(data) {
        this.log("Sending data to Collector on " + this.address + ":" + this.port);

        try {
            let result = await this.sendData(data);
            console.log(result);
        } catch (error) {
            console.log(error);
        }
    }

}

export default Client;