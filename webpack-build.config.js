var htmlWebpackPlugin 	= require('html-webpack-plugin');

module.exports = {
    entry: './frontend/index.js',
    output: {
        path: __dirname + '/dist/frontend',
        filename: 'bundle.js'
    },
    plugins: [
        new htmlWebpackPlugin({
            template: 'frontend/index.tpl.html',
            inject: 'body',
            filename: 'index.html'
        })
    ],
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: [/node_modules/, /dist/],
                loader: 'babel',
                query: { presets: [ 'es2015', 'react' ] }

            },
            {
                test: /\.css$/,
                exclude: [/node_modules/, /dist/],
                loaders: [
                    'style?sourceMap',
                    'css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'
                ]
            }
        ]
    }
};