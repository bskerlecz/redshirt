import SystemLoad from '../../backend/sender/SystemLoad'

describe('SystemLoad', () => {

    it('should throw error when no params passed', () => {
        expect(() => {
            new SystemLoad();
        }).not.toThrowError(ReferenceError);

        expect(() => {
            new SystemLoad();
        }).toThrowError(/Missing required argument/);

    });

    it('should require client injected', () => {
        expect(() => {
            new SystemLoad({});
        }).toThrowError(/Missing required property/);
    });

    it('should require metric-source-id paramter to be passed', () => {
        const mockClient = jest.fn();
        let client = new mockClient();

        expect(() => {
            let systemLoad = new SystemLoad({client: client});
            systemLoad.send();
        }).toThrowError(/metricSourceId/);

    });

    it('should parse /proc/loadavg output', () => {
        const sampleOutput = '3.33 2.90 2.70 4/236 26172';
        const client = jest.fn();

        const systemLoad = new SystemLoad({client: client});
        let data = systemLoad.parseLoadAvg(sampleOutput);

        expect(data.constructor).toBe(Array);
        expect(data.length).toBe(3);
        expect(data[0]).toBe(3.33);
        expect(data[1]).toBe(2.9);
        expect(data[2]).toBe(2.7);
    });
});