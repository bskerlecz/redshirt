import HddTemp from '../../backend/sender/HddTemp'

describe('HddTemp', () => {

    it('should throw error when no params passed', () => {
        expect(() => {
            new HddTemp();
        }).not.toThrowError(ReferenceError);

        expect(() => {
            new HddTemp();
        }).toThrowError(/Missing required argument/);

    });

    it('should require client injected', () => {
        expect(() => {
            new HddTemp({});
        }).toThrowError(/Missing required property/);
    });

    it('should require metric-source-id paramter to be passed', () => {
        const mockClient = jest.fn();
        let client = new mockClient();

        expect(() => {
            let hddTemp = new HddTemp({client: client});
            hddTemp.send();
        }).toThrowError(/metricSourceId/);

    });

    it('should get hddtemp data through a socket', () => {
        const mockClient = jest.fn();
        let client = new mockClient();

        expect(() => {
            let hddTemp = new HddTemp({client: client});
            hddTemp.hddtempCli = 'nonexistenthddtemp';
            hddTemp.send('example-source-1');
        }).not.toThrow();

    });

    it('should parse hddtemp daemon output', () => {
        const sampleOutput = '|/dev/sda|WDC WD20EARX-00PASB0|27|C||/dev/sdc|SAMSUNG HD160HJ|22|C||/dev/sdb|WDC WD20EARX-00PASB0|26|C|';
        const client = jest.fn();

        const hddTemp = new HddTemp({client: client});
        let data = hddTemp.collectData(sampleOutput, "||", "|");

        expect(data.constructor).toBe(Array);
        expect(data.length).toBe(3);
        expect(data[0].device).toBe('/dev/sda');
        expect(data[1].name).toBe('SAMSUNG HD160HJ');
    });
});