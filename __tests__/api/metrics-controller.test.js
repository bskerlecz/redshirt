import { MetricsController } from '../../backend/api/Controllers';

jest.mock('../../backend/collector/Storage');
import Storage from '../../backend/collector/Storage';

const testSourceId = 'sample-foo-source';
const testData = {
    'sample-foo-source' : [
        {},
        {},
        {},
        {}
    ]
};

// Mock sample data
Storage.mockImplementation(() => {
    return {
        listMetrics: (sourceId) => {
            return new Promise((resolve, reject) => {
                resolve(testData[sourceId]);
            });
        }
    };
});

describe("MetricsController", () => {

    it("should not work without storage", () => {
        expect(() => {
            new MetricsController({});
        }).toThrowError(/Invalid or missing 'storage'/);

        const mockStorage = jest.fn();
        let storage = new mockStorage();

        expect(() => {
            new MetricsController({storage: mockStorage});
        }).not.toThrowError(/Invalid or missing 'storage'/);

    });

    it("should return data for current day if no interval specified", async () => {
        let storage = new Storage();
        let metricsController = new MetricsController({storage: storage});

        let result = await metricsController.index({id: testSourceId});

        expect(result.length).toEqual(1);
    });

    /*
    it("should return data for last 7 days if interval 7d specified but no date", () => {
        expect(true).toBeFalsy();
    });

    it("should return data for last 30 days if interval 30d specified but no date", () => {
        expect(true).toBeFalsy();
    });

    it("should return data for given day if interval 1d given or not given but date is specified", () => {
        expect(true).toBeFalsy();
    });

    it("should return data for given 7d if interval is 7 starting with the specified date", () => {
        expect(true).toBeFalsy();
    });

    it("should return data for given 30d if interval is 30 days starting with the specified date", () => {
        expect(true).toBeFalsy();
    });
    */

});